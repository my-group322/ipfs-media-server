package config

import (
	"github.com/europa-dev/europa-media-server/apperrors"
)

type Config struct {
	Port                       int
	AWSS3Region                string  `split_words:"true"`
	AWSS3AssetsBucketName      string  `split_words:"true"`
	AWSS3ThumbAssetsBucketName string  `split_words:"true"`
	AWSS3AssetsBucketPrefix    string  `split_words:"true"`
	AWSS3Key                   string  `split_words:"true"`
	AWSS3Secret                string  `split_words:"true"`
	AWSSqsKey                  string  `split_words:"true"`
	AWSSqsSecret               string  `split_words:"true"`
	AWSSqsRegion               string  `split_words:"true"`
	ApiKey                     string  `split_words:"true"`
	MediaUploadQueueUrl        string  `split_words:"true"`
	PipedIpfsDownloadEndpoints string  `split_words:"true"`
	IpfsEndpointsRateLimit     float64 `split_words:"true"`
}

type Source interface {
	Load() (Config, apperrors.ApplicationError)
}

func Load(source Source) (Config, apperrors.ApplicationError) {
	return source.Load()
}

func AutoLoad() (Config, apperrors.ApplicationError) {
	envSrc := EnvSource{
		Prefix: "",
	}
	cfg, err := Load(envSrc)
	if err != nil {
		return cfg, err
	}

	return cfg, nil
}
