package config

import (
	"github.com/europa-dev/europa-media-server/apperrors"
	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
)

type EnvSource struct {
	Prefix string
}

func (es EnvSource) Load() (Config, apperrors.ApplicationError) {
	_ = godotenv.Load("secrets/.env")
	_ = godotenv.Load(".env")

	var c Config
	pErr := envconfig.Process(es.Prefix, &c)
	if pErr != nil {
		return c, apperrors.NewErrInternal("envconfig.Process return error", pErr)
	}

	return c, nil
}
