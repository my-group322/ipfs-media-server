package service

import (
	"context"
	"strings"
	"sync"

	"github.com/europa-dev/europa-media-server/apperrors"
	"github.com/europa-dev/europa-media-server/config"
	"github.com/europa-dev/europa-media-server/infrastructure/ipfs"
)

type (
	ServiceGetter struct {
		cfg                config.Config
		mediaClient        MediaClient
		messageQueueClient MessageQueueClient
	}
)

func NewServiceGetter(cfg config.Config, mediaClient MediaClient, messageQueueClient MessageQueueClient) *ServiceGetter {
	return &ServiceGetter{
		cfg:                cfg,
		mediaClient:        mediaClient,
		messageQueueClient: messageQueueClient,
	}
}

func (s ServiceGetter) GetMediaService() *MediaService {
	return NewMediaService(s.mediaClient, s.messageQueueClient)
}

func (s ServiceGetter) Sync(ctx context.Context) apperrors.ApplicationError {
	wg := sync.WaitGroup{}

	syncMediaUploadersCtx, syncMediaUploadersCtxDone := context.WithCancel(ctx)
	defer syncMediaUploadersCtxDone()

	ipfsEndpoints := strings.Split(s.cfg.PipedIpfsDownloadEndpoints, "|")

	for _, endpoint := range ipfsEndpoints {
		wg.Add(1)
		go s.GetMediaService().SyncMediaByIpfsProvider(syncMediaUploadersCtx, ipfs.NewProvider(endpoint, s.cfg.IpfsEndpointsRateLimit))
	}

	wg.Wait()

	return nil
}
