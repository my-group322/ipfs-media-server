package service

import (
	"context"
	"fmt"

	"github.com/europa-dev/europa-media-server/apperrors"
	"github.com/europa-dev/europa-media-server/entities"
	"github.com/europa-dev/europa-media-server/infrastructure/logger"
	"github.com/ldez/mimetype"
)

type MediaClient interface {
	MediaExists(ctx context.Context, mediaName string, smallSize bool) (found bool, link string, err apperrors.ApplicationError)
	PutMedia(ctx context.Context, mediaName string, data []byte, contentType string) (link string, err apperrors.ApplicationError)
}

type MessageQueueClient interface {
	ProduceMediaUpload(ctx context.Context, fileLink string, fileType entities.FileType) apperrors.ApplicationError
	ConsumeMediaUpload(ctx context.Context, handler func(ctx context.Context, fileLink string, fileType entities.FileType) apperrors.ApplicationError) apperrors.ApplicationError
}

type IpfsProvider interface {
	GetData(ctx context.Context, dataLink string) ([]byte, string, apperrors.ApplicationError)
	ExtractImageFromMetadata(data []byte) (string, apperrors.ApplicationError)
}

type (
	MediaService struct {
		client             MediaClient
		messageQueueClient MessageQueueClient
	}
)

func NewMediaService(client MediaClient, messageQueueClient MessageQueueClient) *MediaService {
	return &MediaService{
		client:             client,
		messageQueueClient: messageQueueClient,
	}
}

func (s MediaService) GetMediaLink(ctx context.Context, name string, smallSize bool) (link string, err apperrors.ApplicationError) {
	found, link, err := s.client.MediaExists(ctx, name, smallSize)
	if err != nil {
		return "", err
	}

	if !found {
		return "", apperrors.NewErrNotFound(apperrors.ErrorNotFoundMedia, "media with such name not found")
	}

	return link, nil
}

func (s MediaService) ScheduleUploadMedia(ctx context.Context, metadataLinks []string) apperrors.ApplicationError {
	for i := range metadataLinks {
		if err := s.messageQueueClient.ProduceMediaUpload(ctx, metadataLinks[i], entities.FileTypeImage); err != nil {
			return err
		}
	}
	return nil
}

func (s MediaService) SyncMediaByIpfsProvider(ctx context.Context, ipfsProvider IpfsProvider) {
	for {
		if ctx.Err() != nil {
			return
		}

		err := s.messageQueueClient.ConsumeMediaUpload(ctx, func(ctx context.Context, fileLink string, fileType entities.FileType) apperrors.ApplicationError {
			found, _, err := s.client.MediaExists(ctx, fileLink, false)
			if err != nil {
				return err
			}

			if found {
				return nil
			}

			file, contentType, err := ipfsProvider.GetData(ctx, fileLink)
			if err != nil {
				return err
			}

			// content type assertions
			if (fileType == entities.FileTypeJson && contentType != mimetype.ApplicationJSON) || (fileType == entities.FileTypeImage && contentType != mimetype.ImagePng && contentType != mimetype.ImageJpeg) {
				return apperrors.NewErrInternal(fmt.Sprintf("content type assertion failed, expected %s, but got %s", fileType, contentType), nil)
			}

			_, err = s.client.PutMedia(ctx, fileLink, file, contentType)
			if err != nil {
				return err
			}

			if fileType == entities.FileTypeJson {
				image, err := ipfsProvider.ExtractImageFromMetadata(file)
				if err != nil {
					return err
				}

				return s.messageQueueClient.ProduceMediaUpload(ctx, image, entities.FileTypeImage)
			}

			return nil
		})
		if err != nil {
			logger.Get(ctx).LogAppError(apperrors.NewErrInternal("can't consume media upload", err))
		}
	}
}
