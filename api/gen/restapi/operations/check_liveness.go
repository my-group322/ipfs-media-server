// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"net/http"

	"github.com/go-openapi/runtime/middleware"
)

// CheckLivenessHandlerFunc turns a function with the right signature into a check liveness handler
type CheckLivenessHandlerFunc func(CheckLivenessParams) middleware.Responder

// Handle executing the request and returning a response
func (fn CheckLivenessHandlerFunc) Handle(params CheckLivenessParams) middleware.Responder {
	return fn(params)
}

// CheckLivenessHandler interface for that can handle valid check liveness params
type CheckLivenessHandler interface {
	Handle(CheckLivenessParams) middleware.Responder
}

// NewCheckLiveness creates a new http.Handler for the check liveness operation
func NewCheckLiveness(ctx *middleware.Context, handler CheckLivenessHandler) *CheckLiveness {
	return &CheckLiveness{Context: ctx, Handler: handler}
}

/* CheckLiveness swagger:route GET /health checkLiveness

CheckLiveness check liveness API

*/
type CheckLiveness struct {
	Context *middleware.Context
	Handler CheckLivenessHandler
}

func (o *CheckLiveness) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	route, rCtx, _ := o.Context.RouteInfo(r)
	if rCtx != nil {
		*r = *rCtx
	}
	var Params = NewCheckLivenessParams()
	if err := o.Context.BindValidRequest(r, route, &Params); err != nil { // bind params
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}

	res := o.Handler.Handle(Params) // actually handle the request
	o.Context.Respond(rw, r, route.Produces, route, res)

}
