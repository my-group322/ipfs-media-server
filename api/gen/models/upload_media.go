// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"

	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// UploadMedia upload media
//
// swagger:model UploadMedia
type UploadMedia struct {

	// metadata links
	MetadataLinks []string `json:"metadataLinks"`
}

// Validate validates this upload media
func (m *UploadMedia) Validate(formats strfmt.Registry) error {
	return nil
}

// ContextValidate validates this upload media based on context it is used
func (m *UploadMedia) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *UploadMedia) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *UploadMedia) UnmarshalBinary(b []byte) error {
	var res UploadMedia
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
