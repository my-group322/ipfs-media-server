package rest

import (
	"context"

	"github.com/europa-dev/europa-media-server/api/gen/restapi/operations"
	"github.com/europa-dev/europa-media-server/api/rest/errors"
	"github.com/europa-dev/europa-media-server/apperrors"
	"github.com/europa-dev/europa-media-server/infrastructure/logger"
	"github.com/go-openapi/runtime/middleware"
)

type mediaService interface {
	GetMediaLink(ctx context.Context, name string, smallSize bool) (link string, err apperrors.ApplicationError)
	ScheduleUploadMedia(ctx context.Context, fileLinks []string) apperrors.ApplicationError
}

type getMediaHandler struct {
	mediaService mediaService
}

func (h *getMediaHandler) Handle(params operations.GetMediaParams) middleware.Responder {
	ctx := params.HTTPRequest.Context()

	location, err := h.mediaService.GetMediaLink(ctx, params.Name, params.SmallSize)

	if err != nil {
		if err.IsInternalError() {
			logger.Get(ctx).LogAppErrorWithMessage(err, "mediaService return error")
			return operations.NewGetMediaInternalServerError().WithPayload(errors.ApplicationError(err))
		}

		return operations.NewGetMediaBadRequest().WithPayload(errors.ApplicationError(err))
	}

	return operations.NewGetMediaFound().WithLocation(location)
}
