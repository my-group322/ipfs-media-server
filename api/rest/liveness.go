package rest

import (
	"github.com/europa-dev/europa-media-server/api/gen/restapi/operations"
	"github.com/go-openapi/runtime/middleware"
)

type livenessHandler struct {
}

func (lc *livenessHandler) Handle(params operations.CheckLivenessParams) middleware.Responder {
	return operations.NewCheckLivenessOK()
}
