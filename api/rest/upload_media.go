package rest

import (
	"github.com/europa-dev/europa-media-server/api/gen/restapi/operations"
	"github.com/europa-dev/europa-media-server/api/rest/errors"
	"github.com/europa-dev/europa-media-server/infrastructure/logger"
	"github.com/go-openapi/runtime/middleware"
)

type uploadMediaHandler struct {
	mediaService mediaService
}

func (h *uploadMediaHandler) Handle(params operations.UploadMediaParams) middleware.Responder {
	ctx := params.HTTPRequest.Context()

	err := h.mediaService.ScheduleUploadMedia(ctx, params.UploadMediaData.MetadataLinks)

	if err != nil {
		if err.IsInternalError() {
			logger.Get(ctx).LogAppErrorWithMessage(err, "mediaService return error")
			return operations.NewUploadMediaInternalServerError().WithPayload(errors.ApplicationError(err))
		}

		return operations.NewUploadMediaBadRequest().WithPayload(errors.ApplicationError(err))
	}

	return operations.NewUploadMediaOK()
}
