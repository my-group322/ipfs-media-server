package rest

import (
	"github.com/europa-dev/europa-media-server/api/gen/restapi"
	"github.com/europa-dev/europa-media-server/api/gen/restapi/operations"
	"github.com/europa-dev/europa-media-server/apperrors"
	"github.com/europa-dev/europa-media-server/infrastructure/auth"
	"github.com/europa-dev/europa-media-server/service"
	"github.com/go-openapi/loads"
)

// SetHandlers initializes the API handlers with underlying services.
func SetHandlers(services *service.ServiceGetter, authenticator auth.ApiKeyAuth, serv *operations.MediaServerAPI) {
	serv.TokenAuth = authenticator.ValidateHeader
	serv.CheckLivenessHandler = &livenessHandler{}

	serv.GetMediaHandler = &getMediaHandler{mediaService: services.GetMediaService()}
	serv.UploadMediaHandler = &uploadMediaHandler{mediaService: services.GetMediaService()}
}

func InitAPI(services *service.ServiceGetter, authenticator auth.ApiKeyAuth) (*operations.MediaServerAPI, apperrors.ApplicationError) {
	swaggerSpec, err := loads.Analyzed(restapi.SwaggerJSON, "")
	if err != nil {
		return nil, apperrors.NewErrInternal("can not load swagger spec", err)
	}
	apiServer := operations.NewMediaServerAPI(swaggerSpec)

	// pass services instance to API handlers
	SetHandlers(services, authenticator, apiServer)

	return apiServer, nil
}
