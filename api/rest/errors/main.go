package errors

import (
	"errors"
	"net/http"

	"github.com/europa-dev/europa-media-server/api/gen/models"
	"github.com/europa-dev/europa-media-server/apperrors"
)

// ApplicationError returns response payload for 404 code
func ApplicationError(err apperrors.ApplicationError) *models.Error {
	if err == nil {
		return &models.Error{
			Code:  int64(apperrors.ErrorInternalError),
			Value: apperrors.ErrorInternalError.ToValue(),
		}
	}

	restError := &models.Error{
		Code:  int64(err.GetCode()),
		Value: err.GetValue(),
	}
	if !err.IsInternalError() {
		restError.Desc = err.GetDesc()
	}

	return restError
}

type ErrBadAuth struct{}

func IsErrBadAuth(err error) bool {
	return errors.As(err, &ErrBadAuth{})
}

func (e ErrBadAuth) Error() string {
	return "bad_auth"
}

// ForbiddenError returns response payload for 403 code
func ForbiddenError() *models.Error {
	return &models.Error{
		Code:  int64(apperrors.ErrorForbidden),
		Value: apperrors.ErrorForbidden.ToValue(),
		Desc:  "Not enough permissions to perform this request",
	}
}

// BadSignatureError returns response for bad signature case (400 code)
func BadSignatureError() *models.Error {
	return &models.Error{
		Code: http.StatusUnauthorized,
		Desc: "Bad signature",
	}
}

// UnknownAPITokenError returns response for unknown public key of api token case (400 code)
func UnknownAPITokenError() *models.Error {
	return &models.Error{
		Code: http.StatusUnauthorized,
		Desc: "Unknown API token",
	}
}

// BadNonceError returns response for bad nonce case (400 code)
func BadNonceError() *models.Error {
	return &models.Error{
		Code: http.StatusUnauthorized,
		Desc: "Bad nonce",
	}
}
