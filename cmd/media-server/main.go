package main

import (
	"context"

	credentialsv2 "github.com/aws/aws-sdk-go-v2/credentials"
	"github.com/aws/aws-sdk-go-v2/service/sqs"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/europa-dev/europa-media-server/api/gen/restapi"
	"github.com/europa-dev/europa-media-server/api/rest"
	"github.com/europa-dev/europa-media-server/config"
	"github.com/europa-dev/europa-media-server/infrastructure/auth"
	"github.com/europa-dev/europa-media-server/infrastructure/logger"
	"github.com/europa-dev/europa-media-server/infrastructure/media"
	"github.com/europa-dev/europa-media-server/infrastructure/messagequeue"
	"github.com/europa-dev/europa-media-server/service"
)

func main() {
	rootCtx, rootCancel := context.WithCancel(context.Background())
	defer rootCancel()
	rootCtx, rootLog := logger.GetLoggerContext(rootCtx)

	cfg, aErr := config.AutoLoad()
	if aErr != nil {
		rootLog.Fatalln(aErr)
	}

	sess, sErr := session.NewSession(&aws.Config{
		Credentials: credentials.NewStaticCredentials(cfg.AWSS3Key, cfg.AWSS3Secret, ""),
		Region:      aws.String(cfg.AWSS3Region)})
	if sErr != nil {
		rootLog.Fatalf("failed to init aws s3 session: %s", sErr)
	}

	sqsClient := sqs.New(sqs.Options{
		Credentials: credentialsv2.NewStaticCredentialsProvider(
			cfg.AWSSqsKey,
			cfg.AWSSqsSecret,
			"",
		),
		Region: cfg.AWSSqsRegion,
	})
	messageQueueClient := messagequeue.NewClient(sqsClient, cfg.MediaUploadQueueUrl)

	mediaClient := media.NewClient(s3.New(sess), cfg.AWSS3Region, cfg.AWSS3AssetsBucketName, cfg.AWSS3ThumbAssetsBucketName, cfg.AWSS3AssetsBucketPrefix)

	services := service.NewServiceGetter(cfg, mediaClient, messageQueueClient)

	authenticator := auth.NewApiKeyAuth(cfg.ApiKey)

	syncCtx, syncDone := context.WithCancel(rootCtx)
	defer syncDone()

	go func() {
		syncErr := services.Sync(syncCtx)
		if syncErr != nil {
			rootLog.Fatalln(syncErr)
		}
	}()

	api, apiErr := rest.InitAPI(services, authenticator)
	if apiErr != nil {
		rootLog.Fatalln(apiErr)
	}
	server := restapi.NewServer(api)
	server.ConfigureAPI()

	defer func() {
		if err := server.Shutdown(); err != nil {
			rootLog.Fatalln(err)
		}
	}()

	server.Port = cfg.Port
	if err := server.Serve(); err != nil {
		rootLog.Fatalln(err)
	}
}
