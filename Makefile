# Variables
LATEST_COMMIT := $$(git rev-parse HEAD)
VERSION?=latest
PROJECT?=europa-media-server
DOCKER_IMAGE_HOST?=355655830930.dkr.ecr.eu-central-1.amazonaws.com

.PHONY: help
help: ## Show this help
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)
%:
	@:

.PHONY: image
image: ## Build application docker image.
	docker build -f ./Dockerfile -t $(DOCKER_IMAGE_HOST)/$(PROJECT):$(VERSION) . && docker build -f ./Dockerfile -t $(DOCKER_IMAGE_HOST)/$(PROJECT):latest .

.PHONY: push_image
push_image: ## Push application docker image.
	docker push $(DOCKER_IMAGE_HOST)/$(PROJECT):$(VERSION) && docker push $(DOCKER_IMAGE_HOST)/$(PROJECT):latest

.PHONY: docker_image
docker_image: ## Build and push all necessary docker images.
	make image push_image

.PHONY: generate_api
generate_api:
	rm -Rf ./api/gen/models/*
	rm -Rf ./api/gen/restapi/operations/*
	swagger generate server -t api/gen -f ./api/swagger/swagger.yml --exclude-main -A media-server -P models.Principal

.PHONY: production_bin
production_bin:
	GOPRIVATE=claim/* GOOS=linux CGO_ENABLED=0 go build -a -ldflags="-w -s" ./cmd/media-server

.PHONY: serve_ui
serve_ui:
	swagger serve api/swagger/swagger.yml