package apperrors

type ErrValidation struct {
	ErrBase
}

func NewErrValidation(desc string, err error) ErrValidation {
	e := ErrValidation{
		ErrBase: ErrBase{
			code:    ErrorValidation,
			context: make(map[string]interface{}),
			desc:    desc,
		},
	}

	e.addSourceContext(captureStack())
	e.addErrorContext(err)

	return e
}
