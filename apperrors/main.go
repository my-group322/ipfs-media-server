package apperrors

import (
	"fmt"
	"reflect"
	"runtime"
	"strings"
)

type (
	ErrCode         int
	ErrCodeNotFound ErrCode
	ErrCodeExchange ErrCode

	ErrBase struct {
		code    ErrCode
		desc    string
		context map[string]interface{}
	}

	ApplicationError interface {
		Error() string
		GetCode() ErrCode
		GetValue() string
		GetDesc() string
		GetContext() map[string]interface{}
		AddContext(key string, value interface{})
		IsInternalError() bool
		IsNotFoundError() bool
		GetOriginalError() error
	}
)

const (
	ErrContextSource = "source"
	ErrContextError  = "error"
)

var (
	// ErrorInternalError unhandled errors (internal)
	ErrorBadAuth       ErrCode = 7000
	ErrorInternalError ErrCode = 8000

	ErrorNetwork ErrCode = 8004

	// ErrorForbidden handled errors
	ErrorForbidden ErrCode = 9000

	ErrorValidation ErrCode = 9100
	ErrorDuplicate  ErrCode = 9101

	ErrorNotFoundMedia ErrCodeNotFound = 4041
)

var errCodeValues = map[ErrCode]string{
	// internal errors
	ErrorBadAuth:                "bad_auth",
	ErrorInternalError:          "internal",
	ErrorNetwork:                "network",
	ErrorForbidden:              "forbidden",
	ErrorValidation:             "validation",
	ErrorDuplicate:              "duplicate",
	ErrCode(ErrorNotFoundMedia): "media_not_found",
}

func (e ErrCode) ToValue() string {
	if val, found := errCodeValues[e]; found {
		return val
	}

	return "unhandled_error"
}

// base error service error interface realization
func (e ErrBase) GetCode() ErrCode {
	return e.code
}
func (e ErrBase) GetValue() string {
	return e.code.ToValue()
}
func (e ErrBase) Error() string {
	errDetails := strings.Builder{}
	errDetails.WriteString(e.GetValue())
	if e.desc != "" {
		errDetails.WriteString(": ")
		errDetails.WriteString(e.desc)
	}
	if original := e.GetOriginalError(); original != nil {
		errDetails.WriteString(" original: ")
		errDetails.WriteString(original.Error())
	}

	return errDetails.String()
}

func (e ErrBase) GetDesc() string {
	return e.desc
}
func (e ErrBase) GetContext() map[string]interface{} {
	return e.context
}
func (e ErrBase) AddContext(key string, value interface{}) {
	e.context[key] = value
}
func (e ErrBase) IsInternalError() bool {
	switch e.code {
	case ErrorForbidden,
		ErrorValidation,
		ErrorDuplicate,
		ErrCode(ErrorNotFoundMedia):
		return false
	default:
		return true
	}
}
func (e ErrBase) IsNotFoundError() bool {
	switch e.code {
	default:
		return false
	case ErrCode(ErrorNotFoundMedia):
		return true
	}
}

// GetOriginalError returns the orignal error that was used to create this instance of apperror.
// WARNING: It will return nil, if the original error was nil.
func (e ErrBase) GetOriginalError() error {
	if errData, ok := e.context[ErrContextError]; ok {
		if err, isErr := errData.(error); isErr {
			return err
		}
	}
	return nil
}

func (e ErrBase) addSourceContext(src []string) {
	e.context[ErrContextSource] = src
}
func (e ErrBase) addErrorContext(err error) {
	if err != nil {
		e.context[ErrContextError] = err
	}
}

func captureStack() []string {
	pc := make([]uintptr, 15)
	n := runtime.Callers(3, pc)
	frames := runtime.CallersFrames(pc[:n])
	var stack = make([]string, n-1)
	for i := 0; i < n-1; i++ {
		frame, valid := frames.Next()
		if !valid {
			return stack
		}
		stack[i] = fmt.Sprintf("%s:%d", frame.Function, frame.Line)
	}
	return stack
}

// IsEqual compare two application errors
// Func will compare all application errors data, exclude source field from context
// In case of both errors are nil, will return true
func IsEqual(a, b ApplicationError) bool {
	if a == nil && b == nil {
		return true
	}

	if a == nil || b == nil {
		return false
	}

	if a.GetCode() != b.GetCode() {
		return false
	}
	if a.GetDesc() != b.GetDesc() {
		return false
	}

	// make copies of original contexts and remove source field from copy
	aContext := make(map[string]interface{})
	for k, v := range a.GetContext() {
		aContext[k] = v
	}
	delete(aContext, ErrContextSource)

	bContext := make(map[string]interface{})
	for k, v := range b.GetContext() {
		bContext[k] = v
	}
	delete(bContext, ErrContextSource)

	return reflect.DeepEqual(aContext, bContext)
}
