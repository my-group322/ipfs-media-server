package apperrors

import (
	"errors"
)

type ErrInternal struct {
	ErrBase
}

func NewErrInternal(desc string, err error) ErrInternal {
	e := ErrInternal{
		ErrBase: ErrBase{
			code:    ErrorInternalError,
			context: make(map[string]interface{}),
			desc:    desc,
		},
	}

	e.addSourceContext(captureStack())
	e.addErrorContext(err)

	return e
}

func ToInternalError(err error) (bool, ErrInternal) {
	internalError := ErrInternal{}
	isInternal := errors.As(err, &internalError)

	return isInternal, internalError
}

func NewErrTxComplete(err error) ErrInternal {
	e := ErrInternal{
		ErrBase: ErrBase{
			code:    ErrorInternalError,
			context: make(map[string]interface{}),
			desc:    "tx complete error",
		},
	}

	e.addSourceContext(captureStack())
	e.addErrorContext(err)

	return e
}
