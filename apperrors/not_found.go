package apperrors

type ErrNotFound struct {
	ErrBase
}

func NewErrNotFound(code ErrCodeNotFound, desc string) ErrNotFound {
	e := ErrNotFound{
		ErrBase: ErrBase{
			code:    ErrCode(code),
			context: make(map[string]interface{}),
			desc:    desc,
		},
	}

	e.addSourceContext(captureStack())

	return e
}
