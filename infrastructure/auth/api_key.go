package auth

import (
	"github.com/europa-dev/europa-media-server/api/gen/models"
	gerrors "github.com/go-openapi/errors"
)

type ApiKeyAuth struct {
	secret string
}

func NewApiKeyAuth(secret string) ApiKeyAuth {
	return ApiKeyAuth{
		secret: secret,
	}
}

func (a ApiKeyAuth) ValidateHeader(header string) (*models.Principal, error) {
	if header == a.secret {
		return nil, nil
	}
	return nil, gerrors.New(401, "invalid key")
}
