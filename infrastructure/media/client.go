package media

import (
	"bytes"
	"context"
	"fmt"
	"image"
	"image/jpeg"
	"image/png"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/europa-dev/europa-media-server/apperrors"
	"github.com/ldez/mimetype"
	"github.com/nfnt/resize"
)

const (
	notFoundS3Err       = "NotFound"
	mediaLocationFormat = "https://s3.amazonaws.com/%s/%s"
	publicReadACL       = "public-read"
)

type Client struct {
	client                *s3.S3
	region                string
	originalBucketName    string
	thumbBucketName       string
	objectsLocationPrefix string
}

func NewClient(client *s3.S3, region, originalBucketName, thumbBucketName, objectsLocationPrefix string) Client {
	return Client{
		client:                client,
		region:                region,
		originalBucketName:    originalBucketName,
		objectsLocationPrefix: objectsLocationPrefix,
		thumbBucketName:       thumbBucketName,
	}
}

func (c Client) MediaExists(ctx context.Context, mediaName string, smallSize bool) (found bool, link string, err apperrors.ApplicationError) {
	key := c.objectsLocationPrefix + mediaName

	bucket := c.originalBucketName
	if smallSize {
		bucket = c.thumbBucketName
	}

	_, gErr := c.client.HeadObjectWithContext(ctx, &s3.HeadObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	})
	if gErr == nil {
		return true, fmt.Sprintf(mediaLocationFormat, bucket, key), nil
	}

	awsError, ok := gErr.(awserr.Error)
	if !ok {
		return false, "", apperrors.NewErrInternal("can't check if media exists, returned error is not awserr.Error type", gErr)
	}

	if awsError.Code() != notFoundS3Err {
		return false, "", apperrors.NewErrInternal("can't check if media exists, client returned error", gErr)
	}

	return false, "", nil
}

func (c Client) PutMedia(ctx context.Context, mediaName string, data []byte, contentType string) (link string, err apperrors.ApplicationError) {
	key := c.objectsLocationPrefix + mediaName

	_, pErr := c.client.PutObjectWithContext(ctx, &s3.PutObjectInput{
		Bucket:      aws.String(c.originalBucketName),
		Key:         aws.String(key),
		Body:        bytes.NewReader(data),
		ACL:         aws.String(publicReadACL),
		ContentType: aws.String(contentType),
	})
	if pErr != nil {
		return "", apperrors.NewErrInternal("put object to original bucket returned error", pErr)
	}

	// doesn't need resizing
	if contentType != mimetype.ImagePng && contentType != mimetype.ImageJpeg {
		return fmt.Sprintf(mediaLocationFormat, c.originalBucketName, key), nil
	}

	img, _, dErr := image.Decode(bytes.NewReader(data))
	if dErr != nil {
		return "", apperrors.NewErrInternal("failed to decode image", dErr)
	}

	m := resize.Resize(400, 0, img, resize.Lanczos3)

	resizedImg := &bytes.Buffer{}
	if contentType == mimetype.ImagePng {
		pErr = png.Encode(resizedImg, m)
		if pErr != nil {
			return "", apperrors.NewErrInternal("failed to encode png", pErr)
		}
	} else {
		pErr = jpeg.Encode(resizedImg, m, nil)
		if pErr != nil {
			return "", apperrors.NewErrInternal("failed to encode jpeg", pErr)
		}
	}

	_, pErr = c.client.PutObjectWithContext(ctx, &s3.PutObjectInput{
		Bucket:      aws.String(c.thumbBucketName),
		Key:         aws.String(key),
		Body:        bytes.NewReader(resizedImg.Bytes()),
		ACL:         aws.String(publicReadACL),
		ContentType: aws.String(contentType),
	})
	if pErr != nil {
		return "", apperrors.NewErrInternal("put object to thumb bucket returned error", pErr)
	}

	return fmt.Sprintf(mediaLocationFormat, c.originalBucketName, key), nil
}
