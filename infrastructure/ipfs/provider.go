package ipfs

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"

	"github.com/europa-dev/europa-media-server/apperrors"
	"github.com/europa-dev/europa-media-server/infrastructure/transport"
	"github.com/ldez/mimetype"
)

const (
	contentTypeHeader = "content-type"
	httpsSchema       = "https://"
)

type Metadata struct {
	Image string `json:"image"`
}

type Provider struct {
	endpoint string
	client   *http.Client
}

func NewProvider(endpoint string, rateLimit float64) *Provider {
	return &Provider{endpoint: httpsSchema + endpoint, client: &http.Client{
		Transport: transport.NewRateLimitTransport(rateLimit, http.DefaultTransport),
	}}
}

func (p Provider) GetData(ctx context.Context, dataLink string) ([]byte, string, apperrors.ApplicationError) {
	link := p.endpoint + "/ipfs/" + dataLink

	r, err := http.NewRequestWithContext(ctx, http.MethodGet, link, nil)
	if err != nil {
		return nil, "", apperrors.NewErrInternal("failed to create request to get ipfs data", err)
	}

	resp, err := p.client.Do(r)
	if err != nil {
		return nil, "", apperrors.NewErrInternal(fmt.Sprintf("http client endpoint %s returned error", p.endpoint), err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, "", apperrors.NewErrInternal(fmt.Sprintf("link %s http client returned error status code %d", link, resp.StatusCode), nil)
	}

	data, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, "", apperrors.NewErrInternal("failed to read response body", err)
	}

	contentType := resp.Header.Get(contentTypeHeader)
	if contentType == "" {
		// http.DetectContentType doesn't detect json
		if isJson(data) {
			contentType = mimetype.ApplicationJSON
		} else {
			contentType = http.DetectContentType(data)
		}
	}

	return data, contentType, nil
}

func (p Provider) ExtractImageFromMetadata(data []byte) (string, apperrors.ApplicationError) {
	var meta Metadata
	err := json.Unmarshal(data, &meta)
	if err != nil {
		return "", apperrors.NewErrInternal("failed to unmarshal metadata", err)
	}

	pUri := strings.Split(meta.Image, "://")
	if len(pUri) != 2 {
		return "", apperrors.NewErrInternal(fmt.Sprintf("can't extract metadata image, bad image uri format: %s", meta.Image), nil)
	}

	if pUri[0] != "ipfs" {
		return "", apperrors.NewErrInternal(fmt.Sprintf("can't extract metadata image, format is not ipfs but %s", pUri[0]), nil)
	}

	return pUri[1], nil
}

func isJson(bytes []byte) bool {
	var js json.RawMessage
	return json.Unmarshal(bytes, &js) == nil
}
