package messagequeue

import (
	"context"
	"encoding/json"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/sqs"
	"github.com/europa-dev/europa-media-server/apperrors"
	"github.com/europa-dev/europa-media-server/entities"
)

type MediaUploadPayload struct {
	FileLink string `json:"fileLink"`
	FileType string `json:"fileType"`
}

type Client struct {
	client               *sqs.Client
	mediaUploadQueueName string
}

func NewClient(client *sqs.Client, mediaUploadQueueName string) Client {
	return Client{client: client, mediaUploadQueueName: mediaUploadQueueName}
}

func (c Client) ProduceMediaUpload(ctx context.Context, fileLink string, fileType entities.FileType) apperrors.ApplicationError {
	payload, err := json.Marshal(MediaUploadPayload{FileLink: fileLink, FileType: string(fileType)})
	if err != nil {
		return apperrors.NewErrInternal("failed to marshal media upload payload", err)
	}

	_, err = c.client.SendMessage(ctx, &sqs.SendMessageInput{
		MessageBody: aws.String(string(payload)),
		QueueUrl:    aws.String(c.mediaUploadQueueName),
	})
	if err != nil {
		return apperrors.NewErrInternal("failed to send message", err)
	}

	return nil
}

func (c Client) ConsumeMediaUpload(ctx context.Context, handler func(ctx context.Context, fileLink string, fileType entities.FileType) apperrors.ApplicationError) apperrors.ApplicationError {
	output, err := c.client.ReceiveMessage(ctx, &sqs.ReceiveMessageInput{
		QueueUrl: aws.String(c.mediaUploadQueueName),
	})
	if err != nil {
		return apperrors.NewErrInternal("failed to receive message from sqs", err)
	}

	if len(output.Messages) == 0 {
		return nil
	}

	var payload MediaUploadPayload
	err = json.Unmarshal([]byte(*output.Messages[0].Body), &payload)
	if err != nil {
		return apperrors.NewErrInternal("failed to unmarshal sqs message", err)
	}

	fileType, err := entities.NewFileTypeFromString(payload.FileType)
	if err != nil {
		return apperrors.NewErrInternal("failed to cast file type", err)
	}

	err = handler(ctx, payload.FileLink, fileType)
	if err != nil {
		return apperrors.NewErrInternal("error handling message", err)
	}

	// acknowledging
	_, err = c.client.DeleteMessage(ctx, &sqs.DeleteMessageInput{
		QueueUrl:      aws.String(c.mediaUploadQueueName),
		ReceiptHandle: output.Messages[0].ReceiptHandle,
	})
	if err != nil {
		return apperrors.NewErrInternal("error deleting sqs message", err)
	}

	return nil
}
