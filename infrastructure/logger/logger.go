package logger

import (
	"context"
	"os"
	"reflect"

	"github.com/europa-dev/europa-media-server/apperrors"
	"github.com/sirupsen/logrus"
)

type ctxKey string

const (
	unauthorizedUser           = "unauthorized"
	redacted                   = "*******"
	tagSensitive               = "sensitive"
	tagJSON                    = "json"
	ctxLoggerKey        ctxKey = "europa-media-logger"
	applicationErrorKey        = "application_error"
)

type LogWrapper struct {
	log *logrus.Entry
}

// todo: implement application errors, use LogFatalAppError instead of Fatalf and Fatalln
func (s *LogWrapper) Fatalf(format string, args ...interface{}) {
	s.log.Fatalf(format, args...)
}
func (s *LogWrapper) Fatalln(args ...interface{}) {
	s.log.Fatalln(args...)
}

func (s *LogWrapper) Warnf(format string, args ...interface{}) {
	s.log.Warnf(format, args...)
}

func (s *LogWrapper) Infof(format string, args ...interface{}) {
	s.log.Infof(format, args...)
}

func (s *LogWrapper) Errorf(format string, args ...interface{}) {
	s.log.Errorf(format, args...)
}

func (s *LogWrapper) Debugf(format string, args ...interface{}) {
	s.log.Debugf(format, args...)
}

func (s *LogWrapper) Tracef(format string, args ...interface{}) {
	s.log.Tracef(format, args...)
}

func (s *LogWrapper) Panicf(format string, args ...interface{}) {
	s.log.Panicf(format, args...)
}

func (s *LogWrapper) SetLevel(level logrus.Level) {
	s.log.Logger.SetLevel(level)
}

func (s *LogWrapper) WithFields(fields logrus.Fields) *LogWrapper {
	s.log = s.log.WithFields(fields)
	return s
}
func (s *LogWrapper) WithField(key string, value interface{}) *LogWrapper {
	s.log = s.log.WithField(key, value)
	return s
}

func (s *LogWrapper) WithParamMap(params map[string]interface{}) *LogWrapper {
	return &LogWrapper{log: s.log.WithFields(params)}
}

func (s *LogWrapper) LogAppErrorWithMessage(err apperrors.ApplicationError, format string, args ...interface{}) {
	log := s.WithAppError(err)
	if err.IsInternalError() {
		log.Errorf(format, args...)
	} else {
		log.Warnf(format, args...)
	}
}
func (s *LogWrapper) LogAppError(err apperrors.ApplicationError) {
	log := s.WithAppError(err)
	if err.IsInternalError() {
		log.Errorf(err.Error())
	} else {
		log.Warnf(err.Error())
	}
}

func (s *LogWrapper) LogFatalAppError(err apperrors.ApplicationError) {
	log := s.WithAppError(err)
	log.Errorf(err.Error())

	os.Exit(int(err.GetCode()))
}

func (s *LogWrapper) WithAppError(err apperrors.ApplicationError) *LogWrapper {
	return &LogWrapper{log: s.log.WithFields(err.GetContext()).WithField(applicationErrorKey, err.Error())}
}

func clearParams(params interface{}) map[string]interface{} {
	result := make(map[string]interface{})
	if params == nil {
		return result
	}

	pValue := reflect.ValueOf(params)
	pType := reflect.TypeOf(params)

	if pValue.Kind() == reflect.Ptr {
		pValue = pValue.Elem()
		pType = pType.Elem()
	}
	if pValue.Kind() != reflect.Struct {
		return result
	}

	for i := 0; i < pValue.NumField(); i++ {
		tag := pType.Field(i).Tag.Get(tagSensitive)
		val := pValue.Field(i)
		if !val.IsValid() || !val.CanInterface() {
			continue
		}

		if pType.Field(i).Tag.Get(tagJSON) == "-" { //if it's not JSONable we don't want it in json logs
			continue
		}

		if tag != "" {
			result[pType.Field(i).Name] = redacted
			continue
		}

		if val.Kind() == reflect.Ptr {
			val = val.Elem()
			// double check that elem is valid reflect value
			if !val.IsValid() || !val.CanInterface() {
				continue
			}
		}

		if val.Kind() == reflect.Struct {
			result[pType.Field(i).Name] = clearParams(val.Interface())
			continue
		}

		if val.Kind() == reflect.Slice {
			values := make([]interface{}, val.Len())
			for j := 0; j < val.Len(); j++ {

				v := val.Index(j)

				if v.Kind() == reflect.Ptr {
					v = v.Elem()
					// double check that elem is valid reflect value
					if !v.IsValid() || !v.CanInterface() {
						continue
					}
				}

				if v.Kind() == reflect.Struct {
					values[j] = clearParams(v.Interface())
				} else {
					values[j] = v.Interface()
				}
			}
			result[pType.Field(i).Name] = values
			continue
		}

		result[pType.Field(i).Name] = val.Interface()
	}

	return result
}

func getUserUUID(user interface{}) string {
	userUUID := unauthorizedUser
	if user == nil {
		return userUUID
	}

	r := reflect.ValueOf(user)
	if r.Kind() == reflect.Ptr {
		r = r.Elem()
	}

	if r.Kind() != reflect.Struct {
		return userUUID
	}

	val := reflect.Indirect(r).FieldByName("UUID")
	if !val.IsValid() {
		return userUUID
	}

	if val.Type().String() != "string" {
		return userUUID
	}

	fieldValue := val.String()
	if fieldValue == "" {
		return userUUID
	}

	return fieldValue
}

func SetupController(ctx context.Context, reqID string, user, params interface{}) (c context.Context, log *LogWrapper) {
	defer func() {
		if r := recover(); r != nil {
			log = &LogWrapper{log: logrus.WithContext(ctx)}
			log.Errorf("panic in setupcontroller: %+v", r)
			c = context.WithValue(ctx, ctxLoggerKey, log)
			return
		}
	}()
	logger := logrus.WithField("request_id", reqID).WithField("user", getUserUUID(user)).WithField("params", clearParams(params))
	log = &LogWrapper{log: logger}
	return context.WithValue(ctx, ctxLoggerKey, log), log
}

func GetLoggerContext(ctx context.Context) (c context.Context, log *LogWrapper) {
	logrus.SetFormatter(&logrus.JSONFormatter{})
	logrus.SetOutput(os.Stdout)

	log = &LogWrapper{log: logrus.WithContext(ctx)}

	return context.WithValue(ctx, ctxLoggerKey, log), log
}

func Get(ctx context.Context) *LogWrapper {
	logValue := ctx.Value(ctxLoggerKey)
	if logValue == nil {
		return &LogWrapper{log: logrus.WithContext(ctx)}
	}
	if log, ok := logValue.(*LogWrapper); ok {
		return log
	}
	return &LogWrapper{log: logrus.WithContext(ctx)}
}
