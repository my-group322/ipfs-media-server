package entities

import "fmt"

type FileType string

const FileTypeJson FileType = "json"
const FileTypeImage FileType = "image"

func NewFileTypeFromString(s string) (FileType, error) {
	switch s {
	case string(FileTypeJson):
		return FileTypeJson, nil
	case string(FileTypeImage):
		return FileTypeImage, nil
	}

	return "", fmt.Errorf("can't cast file type %s", s)
}
